package io.sevengps.sevencms.serviceImpl;

import io.sevengps.sevencms.common.ResponsePageable;
import io.sevengps.sevencms.domain.Bill;
import io.sevengps.sevencms.domain.BillStatus;
import io.sevengps.sevencms.domain.Customer;
import io.sevengps.sevencms.repository.BillRepository;
import io.sevengps.sevencms.repository.CustomerRepository;
import io.sevengps.sevencms.service.CustomerService;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final BillRepository billRepository;

    @Autowired
    public CustomerServiceImpl(final CustomerRepository customerRepository, final BillRepository billRepository) {
        this.customerRepository = customerRepository;
        this.billRepository = billRepository;
    }


    @Override
    public ResponsePageable findByCustomerIdOrMeterNumber(int page, int size, long customerId, long meterNumber) {
        Page<Customer> customerPage = this.customerRepository.findByCustomerIdOrMeterNumber(customerId, meterNumber, PageRequest.of(page - 1, size));
        return new ResponsePageable(page, size, customerPage.getTotalElements(), customerPage.getTotalPages(), customerPage.getContent());
    }

    @Override
    public ResponsePageable getAllCustomer(int page, int size) {

        Page<Customer> customers = this.customerRepository.findAll(PageRequest.of(page - 1, size));
        return new ResponsePageable(page, size, customers.getTotalElements(), customers.getTotalPages(), customers.getContent());
    }

    @Override
    public ResponsePageable searchCustomerBill(int page, int size, long customerId) {
        Page<Bill> bills = this.billRepository.findAllByCustomerId(customerId, PageRequest.of(page - 1, size));
        return new ResponsePageable(page, size, bills.getTotalElements(), bills.getTotalPages(), bills.getContent());
    }

    @Override
    public void importCustomers(MultipartFile files) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {

            if (index > 0) {
                Customer customer = new Customer();

                XSSFRow row = worksheet.getRow(index);

                customer.setCustomerId((long) row.getCell(0).getNumericCellValue());
                customer.setFirstName(row.getCell(1).getStringCellValue());
                customer.setLastName(row.getCell(2).getStringCellValue());
                customer.setMeterNumber((long) row.getCell(3).getNumericCellValue());
                customer.setCity(row.getCell(4).getStringCellValue());
                customer.setPhoneNumber(row.getCell(5).getStringCellValue());

                this.customerRepository.save(customer);
            }
        }
    }
}