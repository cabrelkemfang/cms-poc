package io.sevengps.sevencms.serviceImpl;

import io.sevengps.sevencms.common.ResponsePageable;
import io.sevengps.sevencms.domain.Bill;
import io.sevengps.sevencms.domain.BillStatus;
import io.sevengps.sevencms.errorHandler.ResourceNotFoundException;
import io.sevengps.sevencms.repository.BillRepository;
import io.sevengps.sevencms.service.BillService;
import io.sevengps.sevencms.service.NotificationService;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BillServiceImpl implements BillService {

    private final BillRepository billRepository;
    private final NotificationService notificationService;

    @Autowired
    public BillServiceImpl(final BillRepository billRepository, final NotificationService notificationService) {
        this.billRepository = billRepository;
        this.notificationService = notificationService;
    }

    @Override
    public void importBill(MultipartFile files) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                Bill bill = new Bill();

                XSSFRow row = worksheet.getRow(index);

                bill.setBillNumber((long) row.getCell(0).getNumericCellValue());
                bill.setReadingDate(row.getCell(1).getStringCellValue());
                bill.setBillingDate(row.getCell(2).getStringCellValue());
                bill.setBillAmount(BigDecimal.valueOf(row.getCell(3).getNumericCellValue()));
                bill.setCustomerId((long) row.getCell(4).getNumericCellValue());
                bill.setCustomerName(row.getCell(5).getStringCellValue());
                bill.setMeterNumber((long) row.getCell(6).getNumericCellValue());
                bill.setStatus(BillStatus.valueOf(row.getCell(7).getStringCellValue()));
                bill.setBillAmountBalance(BigDecimal.valueOf(0));

                this.billRepository.save(bill);
            }
        }
    }

    @Override
    public ResponsePageable searchBill(int page, int size, long customerId, long meterNumber, long billNumber) {
        Page<Bill> bills = this.billRepository.findAllByCustomerIdOrMeterNumberOrBillNumber(customerId, meterNumber, billNumber, PageRequest.of(page - 1, size));
        return new ResponsePageable(page, size, bills.getTotalElements(), bills.getTotalPages(), bills.getContent().stream().filter(bill -> (bill.getStatus().equals(BillStatus.UNPAID))).collect(Collectors.toList()));
    }

    @Override
    public ResponsePageable getAllBill(int page, int size) {
        Page<Bill> bills = this.billRepository.findAll(PageRequest.of(page - 1, size));
        return new ResponsePageable(page, size, bills.getTotalElements(), bills.getTotalPages(), bills.getContent().stream().filter(bill -> (bill.getStatus().equals(BillStatus.UNPAID))).collect(Collectors.toList()));
    }

    @Override
    public boolean UpdateBills(long billNumber, double amount) {
        Optional<Bill> bill = this.billRepository.findByBillNumber(billNumber);

        if (bill.isPresent()) {

            if (bill.get().getStatus().equals(BillStatus.PAID)) {
                throw new ResourceNotFoundException("Bill with id " + billNumber + " is Already Paid");

            } else {
                if (bill.get().getBillAmount().compareTo(BigDecimal.valueOf(amount)) == 0) {
                    bill.get().setStatus(BillStatus.PAID);

                    this.billRepository.save(bill.get());

//                    this.notificationService.notificationSms(bill.get().getBillNumber(), bill.get().getBillAmount(), bill.get().getCustomerId());

                } else if (bill.get().getBillAmount().compareTo(BigDecimal.valueOf(amount)) == -1) {
                    throw new ResourceNotFoundException("The amount enter : " + amount + " is grater than the bill amount :" + bill.get().getBillAmount());
                } else {

                    bill.get().setStatus(BillStatus.COMMITED);
                    bill.get().setBillAmountBalance(bill.get().getBillAmount().subtract(BigDecimal.valueOf(amount)));
                    this.billRepository.save(bill.get());

                    Bill newUnpaidBill = new Bill();

                    newUnpaidBill.setBillNumber(bill.get().getBillNumber());
                    newUnpaidBill.setReadingDate(bill.get().getReadingDate());
                    newUnpaidBill.setBillingDate(bill.get().getBillingDate());
                    newUnpaidBill.setBillAmount(bill.get().getBillAmount().subtract(BigDecimal.valueOf(amount)));
                    newUnpaidBill.setCustomerId(bill.get().getCustomerId());
                    newUnpaidBill.setCustomerName(bill.get().getCustomerName());
                    newUnpaidBill.setMeterNumber(bill.get().getMeterNumber());
                    newUnpaidBill.setBillAmountBalance(BigDecimal.valueOf(0));
                    newUnpaidBill.setStatus(BillStatus.UNPAID);

                    this.billRepository.save(newUnpaidBill);
                }
            }
        } else {
            throw new ResourceNotFoundException("Bill with bill Number " + billNumber + " not found");
        }
        return true;
    }
}
