package io.sevengps.sevencms.serviceImpl;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;
import io.sevengps.sevencms.domain.Customer;
import io.sevengps.sevencms.repository.CustomerRepository;
import io.sevengps.sevencms.service.NotificationService;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceImpl implements NotificationService {

    public static final String ACCOUNT_SID = "AC4fe1d431df183793158f268ac579803a";
    public static final String AUTH_TOKEN = "8b60efb25d852063f299a0a4923d8f04";
    public static final String TWILIO_NUMBER = "+16152099286";

    private final CustomerRepository customerRepository;

    public NotificationServiceImpl(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void notificationSms(Long billNumber, BigDecimal amount, Long customerId) {

        Optional<Customer> customer = this.customerRepository.findByCustomerId(customerId);

        try {
            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

            // Build a filter for the MessageList
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Body", "Eneo \n  Mr " + customer.get().getFirstName()+" "+customer.get().getLastName() + "\n You bill with the number :" + billNumber + " \n Amount :" + amount + " \n Have Been paid Successfully .\n Thanks for you confidence"));
            params.add(new BasicNameValuePair("To", "+237675463395")); //Add real number here
            params.add(new BasicNameValuePair("From", TWILIO_NUMBER));

            MessageFactory messageFactory = client.getAccount().getMessageFactory();
            Message message = messageFactory.create(params);

            System.out.println(message.getSid());
        } catch (TwilioRestException e) {
            System.out.println(e.getErrorMessage());
        }
    }
}
