package io.sevengps.sevencms.controller;

import io.sevengps.sevencms.common.ApiResponse;
import io.sevengps.sevencms.common.ResponsePageable;
import io.sevengps.sevencms.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@RequestMapping("bills")
public class BillsController {

    private final BillService billService;

    @Autowired
    public BillsController(final BillService billService) {
        this.billService = billService;
    }

    @GetMapping()
    public ResponsePageable getAllBills(@RequestParam(defaultValue = "1", required = false) int page,
                                        @RequestParam(defaultValue = "50", required = false) int size) {

        return this.billService.getAllBill(page, size);
    }

    @PostMapping()
    public ResponseEntity<ApiResponse> importBills(@RequestParam("file") MultipartFile file) throws IOException {

        this.billService.importBill(file);
        return ResponseEntity.ok(new ApiResponse(HttpStatus.CREATED.value(), "Bills Imported with success"));
    }

    @GetMapping(path = "/{identifier}")
    public ResponsePageable searchBillsByCustomerIdMeterNumberBillNumber(@PathVariable Long identifier,
                                        @RequestParam(defaultValue = "1", required = false) int page,
                                        @RequestParam(defaultValue = "10", required = false) int size) {
        return this.billService.searchBill(page, size, identifier, identifier, identifier);
    }

    @PostMapping(path = "/{billNumber}")
    public ResponseEntity<ApiResponse> updateBillsPayment(@PathVariable Long billNumber,
                                                          @RequestParam() double amount) {
        this.billService.UpdateBills(billNumber, amount);
        return ResponseEntity.ok(new ApiResponse(HttpStatus.CREATED.value(), "Bills Payment Updated with success"));
    }
}
