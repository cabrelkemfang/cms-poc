package io.sevengps.sevencms.controller;

import io.sevengps.sevencms.common.ApiResponse;
import io.sevengps.sevencms.common.ResponsePageable;
import io.sevengps.sevencms.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("customers")
public class CustomersController {

    private final CustomerService customerService;

    @Autowired
    public CustomersController(final CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping()
    public ResponsePageable getAllCustomers(@RequestParam(defaultValue = "1", required = false) int page,
                                            @RequestParam(defaultValue = "50", required = false) int size) {

        return this.customerService.getAllCustomer(page, size);
    }

    @PostMapping()
    public ResponseEntity<ApiResponse> importAccount(@RequestParam("file") MultipartFile file) throws IOException {

        this.customerService.importCustomers(file);
        return ResponseEntity.ok(new ApiResponse(HttpStatus.CREATED.value(), "Customers Imported with success"));
    }

    @GetMapping(path = "/{identifier}")
    public ResponsePageable searchCustomer(@PathVariable Long identifier,
                                           @RequestParam(defaultValue = "1", required = false) int page,
                                           @RequestParam(defaultValue = "10", required = false) int size) {
        return this.customerService.findByCustomerIdOrMeterNumber(page, size, identifier, identifier);
    }

    @GetMapping(path = "/{customerId}/bills")
    public ResponsePageable searchCustomerBill(@PathVariable Long customerId,
                                               @RequestParam(defaultValue = "1", required = false) int page,
                                               @RequestParam(defaultValue = "10", required = false) int size) {
        return this.customerService.searchCustomerBill(page, size, customerId);
    }
}

