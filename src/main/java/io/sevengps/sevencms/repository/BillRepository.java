package io.sevengps.sevencms.repository;

import io.sevengps.sevencms.domain.Bill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface BillRepository extends JpaRepository<Bill, Long> {

    Page<Bill> findAllByCustomerIdOrMeterNumberOrBillNumber(Long customerId, Long meterNumber, Long billNumber, Pageable pageable);

    Page<Bill> findAllByCustomerId(Long customerId, Pageable pageable);

    @Query("SELECT u FROM Bill u  WHERE  u.billNumber = :billNumber and u.status = 1")
    Optional<Bill> findByBillNumber(@Param("billNumber") Long billNumber);

}
