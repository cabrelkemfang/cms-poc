package io.sevengps.sevencms.repository;

import io.sevengps.sevencms.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Page<Customer> findByCustomerIdOrMeterNumber(Long customerId, Long meterNumber, Pageable pageable);

    Optional<Customer> findByCustomerId(Long customerId);
}
