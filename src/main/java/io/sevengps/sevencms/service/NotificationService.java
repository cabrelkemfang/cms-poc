package io.sevengps.sevencms.service;

import java.math.BigDecimal;

public interface NotificationService {

    void notificationSms(Long billNumber, BigDecimal amount, Long customerId);
}
