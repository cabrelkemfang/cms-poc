package io.sevengps.sevencms.service;

import io.sevengps.sevencms.common.ResponsePageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CustomerService {

    ResponsePageable findByCustomerIdOrMeterNumber(int page, int size, long customerId, long meterNumber);

    ResponsePageable getAllCustomer(int page, int size);

    ResponsePageable searchCustomerBill(int page, int size, long customerId);

    void importCustomers(MultipartFile file) throws IOException;
}
