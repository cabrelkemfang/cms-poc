package io.sevengps.sevencms.service;

import io.sevengps.sevencms.common.ResponsePageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;

public interface BillService {

    void importBill(MultipartFile files) throws IOException;

    ResponsePageable searchBill(int page, int size, long customerId, long meterNumber, long billNumber);

    ResponsePageable getAllBill(int page, int size);

    boolean UpdateBills(long billNumber, double amount);
}
