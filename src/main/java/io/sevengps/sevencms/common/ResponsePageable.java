package io.sevengps.sevencms.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponsePageable {

    private int page;

    private int size;

    private long totalOfItems;

    private int totalOfPage;

    private List<?> data;
}
