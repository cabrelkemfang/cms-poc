package io.sevengps.sevencms.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {
    private int code;

    private String message;

    private List<?> data;

    public ApiResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
