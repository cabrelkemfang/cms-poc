package io.sevengps.sevencms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SevencmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SevencmsApplication.class, args);
    }

}
