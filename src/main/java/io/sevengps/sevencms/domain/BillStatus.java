package io.sevengps.sevencms.domain;

public enum BillStatus {
    PAID,
    UNPAID,
    COMMITED
}
