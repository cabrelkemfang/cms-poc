package io.sevengps.sevencms.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "bills")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bill extends AuditModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bill_id")
    private Long billId;

    @Column(name = "bill_number")
    private Long billNumber;

    @Column(name = "reading_date")
    private String readingDate;

    @Column(name = "billing_date")
    private String billingDate;

    @Column(name = "bill_Amount")
    private BigDecimal billAmount;

    @Column(name = "bill_Amount_balance")
    private BigDecimal billAmountBalance;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "meter_number")
    private Long meterNumber;

    @Column
    private BillStatus status;
}
